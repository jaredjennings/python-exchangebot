# exchangebot

This is a program that logs into a Microsoft Exchange account, and
processes messages in certain folders with Python code. It can run as
a Windows service.

# Audience and use

This may be for you if:

- you are in a Windows environment, with Exchange email.
- you have an application which can send information in an email
  message, but can't use more structured means like SNMP, syslog, a
  webhook, or providing an API.
- you can trust that the messages you are going to receive are
  authentic, and that they are not maliciously crafted.
- the proper response is scriptable.

# How it works

You set up an unprivileged service account with an Exchange mailbox,
and rules that sort different kinds of messages into different
folders. exchangebot logs into Exchange with EWS, using
`exchangelib`. It checks for email in the folders, and runs a
*processor* on it: a piece of code which is passed the message. If
processing is successful, exchangebot moves the message to another
folder.

If run as a service, exchangebot stays logged into the server and
periodically checks mail. Credentials are stored using the `keyring`
package; Exchange login using SSPI is supported. Messages are logged
using the Python `logging` framework.

# Security

Email is not a preferable medium for automating things. Anyone can
send an email, and it can say anything. Messages can be spoofed, or
crafted to exploit bugs in parsers. Even when everything else is
right, the content of an email message is usually made for people to
read, not programs.

For these reasons, exchangebot does as little parsing as it can, and
leaves as much security as it can to the email infrastructure.

It's up to you as the user of exchangebot to:

- avoid using exchangebot. Find another way to automate things besides
  email, if possible.
- limit who can send messages that exchangebot will end up reading.
- sort emails using server code before exchangebot sees them.
- securely configure your email servers, and keep them patched.
  
exchangebot does not act on emails itself: it chooses a *processor* to
do so. It's up to you as the user/developer of processor code to:

- avoid using exchangebot. Find another way to automate things besides
  email, if possible.
- avoid parsing where possible; trust the result of the server's
  parsing instead.
- automate carefully. Think about who could make your code run
  properly for the wrong reason.
