# exchangebot configuration

exchangebot does three major things:

1. Connect to an email account.
2. Run some code on messages inside.
3. Log what happens.

These correspond to the three top keys in the exchangebot configuration
file, which is written in YAML. See the `config-example.yaml` for
example values and how the whole file fits together.

## mail

Configuration needed to connect to the mail account. Keys in here:

* `server`: The FQDN of the Exchange server to connect to.
* `primary_smtp_address`: the email address of the mailbox to open.
* `username`: the username to use when connecting. This should be the
  identity of someone with access to the named mailbox. Put the value
  in quotes, and double any backslashes.
* `authn_method`: Can be `password_from_keyring`, `sspi`, or
  `prompt_for_password`. If the password is obtained from the keyring,
  the location used is the server name configured above, and the
  username used is the username configured above. Use `python -m
  keyring`, or `keyring.exe` in the built and installed MSI, to place
  the password in the keyring. `sspi` uses the identity of the
  logged-in user to connect to Exchange, via Kerberos or
  NTLM. `prompt_for_password` does what it says on the tin, as they
  say.

## processes

This is where you tell exchangebot which processes it's going to
automate. The value of `processes` is a dictionary, each of whose keys
is the name of a *processor*. The value is another dictionary, whose
keys are:

* `messages_in`: a list of subfolder names that forms a path to the
  folder where messages to be processed are found.
* `send_through`: an object reference to a subclass of Processor, of
  the form `some.importable.module:Classname`.
* `archive_success`: a list of subfolder names that forms a path to
  the folder where successfully processed messages should be placed.
* `enabled`: true or false. If false, this processor is not used. If
  not given, defaults to true.
* `config`: a thing expressible in YAML which is usable in the context
  of the `send_through` class as `self.config`. Probably a
  dictionary. Contents defined by the processor plugin.

## logging

A logging configuration dictionary; see
https://docs.python.org/3/library/logging.config.html#logging-config-dictschema.
