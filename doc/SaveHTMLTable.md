# SaveHTMLTable plugin

This plugin ingests emails with an HTML attachment of a given exact
name, containing a table. It is generalized from an earlier, more
organization-specific processor, and retains some peculiarities from
its history.

## Configuration

Here's an example configuration with only the parts unique to
SaveHTMLTable. See also [the configuration requirements for
processors in general](CONFIGURATION.md).

```yaml
# ...
processes:
  position_changes:
    send_through: "exchangebot.plugins.save_html_table.processor:SaveHTMLTable"
    config:
      attachment_name: "File with data.htm"
      upper_left_cell_text: "Row ID"
      output_directory: "x:\\output"
      prefix: mycsv
```

With this example configuration, when an email with an attachment
called "File with data.htm" is processed, a table will be sought,
whose upper-left cell contains the text "Row ID." Any tables in the
attachment not having that text in their upper-left cell will be
ignored. The data from this table will be saved in a file called
`x:\output\mycsv-[datetime].csv`. The time part of the filename will
be taken from the date and time when the message was sent.
