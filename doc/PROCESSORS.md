# Writing a processor

Look at `exchangebot/processor.py`. Write a subclass of Processor.

```python
from exchangebot.processor import Processor

class MyCoolProcessor(Processor):
```

Don't override `__init__`; do override `_more_init` if you
need. `self.config` is a dictionary with your configuration inside it,
from exchangebot's configuration file. The `jingle` method helps you
get a password using `keyring`, or raise an appropriate error if it is
missing..

```python
    def _more_init(self):
        server = config['server_name']
        username = config['user']
        password = self.jingle('API password', server, username)
        self.api_connection = connect_to_cool_api(config['server_name'],
                                                  username, password)
```

Now define the `process_one` method, which takes an `exchangelib`
message object as a parameter, and does something with it. You get a
logger called `self.log` to use. If you successfully dealt with the
message and it should be archived, return True. If you didn't
successfully deal with the message and it should be left alone, return
False. (An example of this might be if there is a temporary error
talking to your API, and it may work if you try again later.) Any
exception you raise will be caught and logged, and the message will be
left alone.

```python
    def process_one(self, message):
        self.log.info("Hello world! The subject of the message was %r",
                      message.subject)
        return True
```

# Deployment

Your plugin should live in the exchangebot.plugins package, i.e. in
the `exchangebot/plugins` directory.

Add any required Python packages to the requirements.txt file using
the usual `pip freeze` process. Note that, under Powershell in
Windows, if you redirect the output of `pip freeze`, the file you end
up with may not use the text encoding you expect. You may wish instead
to `pip freeze | Out-File -Encoding UTF8 requirements.txt`.

If your code is a package and not just a module (a directory of py
files and not just one), you may need to modify setupexe.py to include
it.

Now when you `python setupexe.py bdist_msi`, your code will be
included in the install file.
