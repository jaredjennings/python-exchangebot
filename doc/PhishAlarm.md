# PhishAlarm plugin

This plugin processes Potential Phish report emails from the
Proofpoint Security Awareness (formerly Wombat) PhishAlarm plugin for
Outlook. At this writing, it looks for Mimecast URL Protect URLs and
decodes them; and it creates an alert in TheHive for each reported
phish it receives. The configuration you write for it mostly consists
in the details needed to connect to those services.

## Configuration

Here's an example configuration with only the parts unique to
PhishAlarm. See also [the configuration requirements for processors in
general](CONFIGURATION.md).

```yaml
# ...
processes:
  phishalarm:
    send_through: "exchangebot.plugins.phishalarm.processor:PhishAlarmPotentialPhish"
    config:
      hive:
        cacerts_filename: 'd:\\exchangebot\\cacerts.pem'
        endpoint: "https://my.hive.server.example.com:1234"
        api_key_from_keyring_with_username: a_hive_user_with_api_key
      mimecast_api:
        # This is a random UUID given as an example; do not use it verbatim.
        app_id: 4c6d4079-dcb2-4879-80c8-b3ef25d548f9
        base_url: "https://za-api.mimecast.example.com"
        app_key_from_keyring_with_username: 4c6d-app-key
        access_key_from_keyring_with_username: 4c6d-access-key
        secret_key_from_keyring_with_username: 4c6d-secret-key
```

### hive

#### cacerts_filename

The name of a file, accessible at runtime, with CA certificates
concatenated in PEM format, to trust when attempting to connect to
TheHive. If you don't need this, just don't specify it at all.

#### endpoint

TheHive instance.

#### api\_key\_from\_keyring\_with\_username

The Hive connector will fetch the API key using the `keyring` package,
using the `endpoint` value as the site name and this value as the
username. You have to put this value in the keyring.

### mimecast_api

All of these values you obtain when you sign up for an API application
on the Mimecast admin website. All of the `_from_keyring` values you
have to put in the keyring.

## Misc

Trademarks above are the property of their respective owners.
