# Preparation and use

## Prepare to run golem or build installer

You've just cloned this code into a directory. Start a PowerShell, cd
to there, and:

```powershell
    & 'c:\program files\python37\python.exe' -m venv ve
    .\ve\scripts\activate.ps1
    pip install -r requirements.txt
```

NOTE: When you package the installer, it includes `keyring.exe`, which
runs the code from `ve/lib/site-packages/keyring/__main__.py`. This
means you must either make your virtual environment just this way, or
fix up `setupexe.py` before you build the MSI. This is a kludge, but
it results in a working `keyring.exe` on the server where you install
from the MSI package.

## Run the golem once or a few times

```powershell
    .\ve\scripts\activate.ps1
    # edit the configuration
    python -m exchangebot
    # follow the usage help
    # when credentials can't be found, e.g. for https://foo, user bar
    python -m keyring set 'https://foo' bar
    # now paste or type the password
```

## Build an MSI with the software

After the preparations above,

```powershell
    python setupexe.py bdist_msi
```

## Install it as a service

Given:

- a server where you want the exchangebot installed, and have admin
  access.
- a built MSI file.
- an install directory - let us say `D:\exchangebot`.
- a distinguishing name for the exchangebot service instance, such as
  `production`. You can have more than one instance of the service per
  server.
- a non-admin account `CONTOSO\exchbot_account`, which has access to the
  Exchange mailbox that will be checked; read permissions to
  `D:\exchangebot`; and write permissions to any log files you may have
  configured.

Log in as someone who is a local administrator, and double-click the
MSI; or run this PowerShell command (you can do it over a PSSession,
if you have one open).

```powershell
    msiexec /i ...\exchangebot*msi TARGETDIR=D:\exchangebot /qb
```

Write a configuration file. This is best done while running the
exchangebot by one of the above interactive methods. For service use,
make sure any files named in the config file are specified by their full
pathnames.

Install the service: from an elevated command prompt,

```powershell
    D:\exchangebot\exchangebot_service.exe --install production D:\exchangebot\config-production.yaml
```

Go in the `services.msc` management console and change the login
for the service to `CONTOSO\exchbot_account`, and enter its password. Or:

```powershell
    $s = Get-WMIObject -Query "select * from win32_service where name = 'exchangebotproduction'"
    $s.Change($null, $null, $null, $null, $null, $null, 'CONTOSO\exchbot_account', 'the_password')
    $s.StartService()

    # if you don't have the $s variable laying about from the above lines:
    Start-Service exchangebotproduction
```

The service will log startup messages to
`D:\exchangebot\exchangebot_service.log`, and messages during its run
to wherever the configuration file says.

To shake out bugs in the configuration and missing keyring items, run
a powershell as `CONTOSO\exchbot_account` and run `exchangebot.exe`
interactively. If there are any unhandled exceptions causing the
exchangebot service to quit, these are best debugged using
`exchangebot.exe` as well.

Once everything is shaken out, you can start the service and log out!
