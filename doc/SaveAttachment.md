# SaveAttachment plugin

This plugin saves certain attachments to emails. Be sure of the source
of your emails before letting them be processed by this plugin, for
example using inbox rules.

## Configuration

Here's an example configuration with only the parts unique to
SaveAttachment. See also [the configuration requirements for
processors in general](CONFIGURATION.md).

```yaml
# ...
processes:
  save1:
    send_through: "exchangebot.plugins.save_attachment.processor:SaveAttachment"
    config:
      output_directory: 'x:\\files_from_save1'
      filename_prefix: 'save1'
      extension: ".csv"
```

When an attachment whose name ends with the given extension is
processed, the contents of the attachment are written in a file (e.g.)
`x:\files_from_save1\save1-2002-09-27T11-08-00+00-00.csv`: i.e. the
file is in the `output_directory`, its name starts with the
`filename_prefix`, the name contains the date and time when the
message was received, and it has the given `extension`.
