# https://stackoverflow.com/a/32998111 "import setuptools before anything else"
import setuptools

from cx_Freeze import setup, Executable

import pdb

setup(name='exchangebot',
      version='0.24',
      description='Act programmatically on email messages in a Microsoft Exchange account',
      author='Jared Jennings',
      author_email='jjennings@fastmail.fm',
      url='https://gitlab.com/jaredjennings/python-exchangebot',
      license='BSD-2-Clause',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Information Technology',
          'Intended Audience :: Developers',
          'Intended Audience :: System Administrators',
          'License :: OSI Approved :: BSD License',
          'Operating System :: Microsoft :: Windows',
          'Programming Language :: Python :: 3',
          'Topic :: Communications :: Email',
          'Topic :: Security',
      ],
      packages=['exchangebot',
                'exchangebot.service',
                'exchangebot.plugins',
                'exchangebot.plugins.phishalarm'],
      #package_data={'exchangebot': ['cacerts.pem']},
      install_requires=[
          'beautifulsoup4',
          'exchangelib',
          'requests-negotiate-sspi',
          'cx_Freeze',
          'lxml',
          'pyyaml',
          'mimecast-api',
          'pywin32',
          'thehive4py',
          'keyring',
          ],
      options={'build_exe': {
          'packages': [
              'pkg_resources',
              'lxml',
              'gzip',
              '_cffi_backend',
              'win32timezone',
              'exchangebot',
              'exchangebot.service',
              'exchangebot.plugins',
          ],
          'includes': [
              'keyring.backends.chainer',
              'keyring.backends.fail',
              'keyring.backends.kwallet',
              'keyring.backends.null',
              'keyring.backends.OS_X',
              'keyring.backends.SecretService',
              'keyring.backends.Windows',
              'keyring.backends._OS_X_API',
              'exchangebot.service.ServiceHandler',
              'cx_Logging',
          ],
          'excludes': [
              'tcl',
              'tk',
              'tkinter',
          ],
      },
               'bdist_msi': {
                   'initial_target_dir': 'D:\exchangebot',
                   # "used to force removal of any packages created
                   # with the same upgrade code prior to the
                   # installation of this one"
                   'upgrade_code': '{14a9562e-4e55-4508-a914-f697558fe571}',
               },},
      executables=[Executable('exchangebot/__main__.py',
                              targetName='exchangebot.exe'),
                   Executable('exchangebot/service/Config.py',
                              base='Win32Service',
                              targetName='exchangebot_service.exe'),
                   Executable('ve/lib/site-packages/keyring/__main__.py',
                              targetName='keyring.exe'),
      ],
)
