from pytest import raises
from ..processor import Processor

def test_uncaught_exception(mocker):
    """Uncaught exceptions are raised outside the Processor."""
    account = mocker.MagicMock()
    class UnheardOfException(Exception):
        pass
    class UncaughtExceptionRaisingProcessor(Processor):
        def process_one(self, message):
            raise UnheardOfException('gotcha!')
    p = UncaughtExceptionRaisingProcessor(account, from_='foo',
                                          success=mocker.MagicMock(),
                                          config={})
    message = mocker.MagicMock()
    with raises(UnheardOfException):
        p.process_all([message])
        
