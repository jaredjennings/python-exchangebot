import logging
import importlib.resources

def get_processor_class(spec):
    log = logging.getLogger('eb.gp.gpc')
    log.debug('getting %s', spec)
    module_name, class_name = spec.split(':')
    modu = importlib.import_module(module_name)
    clas = getattr(modu, class_name)
    return clas
