"""
Implements a simple service using cx_Freeze.
See below for more information on what methods must be implemented and how they
are called.
"""

import threading
import sys
import logging
import argparse
import importlib.resources
import time
import yaml
from exchangebot.account import account_from_mail_config
from exchangebot.logging import start_logging, finish_logging
from exchangebot.getprocessor import get_processor_class
import random

class Handler(object):

    # no parameters are permitted; all configuration should be placed in the
    # configuration file and handled in the Initialize() method
    def __init__(self):
        self.stopEvent = threading.Event()
        self.stopRequestedEvent = threading.Event()
        self.processors = {}
        self.mail_account = None

    # called when the service is starting
    def Initialize(self, configFileName):
        log = logging.getLogger('eb.SH.In')
        with open(configFileName, 'rt') as cf:
            self.config = yaml.safe_load(cf)
        # in __main__.py we default to no logging
        # configuration. start_logging then sends every log message to
        # stderr. but we must have working logging config in a windows
        # service to get messages out, so we crash if it is not in the
        # config.
        start_logging(self.config['logging'])
        log.info('starting')
        account = account_from_mail_config(self.config['mail'])
        for config_name, pr in self.config['processes'].items():
            if pr.get('enabled', True):
                clas = get_processor_class(pr['send_through'])
                ob = clas(account, from_=pr['messages_in'],
                          success=pr['archive_success'],
                          config=config)
                log.info('processor enabled: %r is %r', config_name, ob)
                self.processors[config_name] = ob
            else:
                log.info('processor disabled: %r', config_name)
        pass

    # called when the service is starting immediately after Initialize()
    # use this to perform the work of the service; don't forget to set or check
    # for the stop event or the service GUI will not respond to requests to
    # stop the service
    def Run(self):
        log = logging.getLogger('eb.SH.Run')
        while True:
            for name, pr in self.processors.items():
                try:
                    pr.process_all()
                    log.debug('finished with %s', name)
                except Exception as e:
                    log.exception('quitting due to uncaught exception')
                    break
            if self.stopRequestedEvent.wait(30):
                break
        log.info('ending')
        finish_logging()
        self.stopEvent.set()

    # called when the service is being stopped by the service manager GUI
    def Stop(self):
        log = logging.getLogger('eb.SH.Stop')
        log.info('stop requested')
        self.stopRequestedEvent.set()
        self.stopEvent.wait()
