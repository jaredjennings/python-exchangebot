import sys
import logging
import argparse
import time
import yaml
from exchangebot.account import account_from_mail_config
from exchangebot.logging import logging_handled
from exchangebot.getprocessor import get_processor_class
import random

p = argparse.ArgumentParser(
    description='Do vaguely smart things with emails.')
p.add_argument('-c', '--config',
               type=argparse.FileType('r', encoding='UTF-8'),
               help='the configuration YAML file',
               required=True)
p.add_argument('-p', '--processor',
               type=str,
               help='which configured processor to run',
               required=True)
p.add_argument('-k', '--keep-running',
               action='store_true',
               help='run processor every 30 seconds for 10 minutes')
args = p.parse_args()
c = yaml.safe_load(args.config)
with logging_handled(c.get('logging', None)):
    log = logging.getLogger('eb.main')
    if args.processor in c['processes']:
        pr = c['processes'][args.processor]
    else:
        log.error('Unknown processor %s. Expected one of the processes '
                  'named under "processes:" in %s, '
                  'i.e. %r. Nothing done.',
                  args.processor, args.config, list(c['processes'].keys()))
        sys.exit(1)
    account = account_from_mail_config(c['mail'])
    start_time = time.time()
    if args.keep_running:
        stop_time = start_time + 60*60
        print('Emitting random dots every time we check: ', end='')
        sys.stdout.flush()
    else:
        # only run once
        stop_time = start_time
    spec = pr['send_through']
    if pr.get('enabled', True):
        clas = get_processor_class(spec)
        ob = clas(account, from_=pr['messages_in'],
                  success=pr['archive_success'],
                  config=pr['config'])
        while True: # we want to check whether to stop only after going once
            try:
                ob.process_all()
                log.debug('finished with %s', spec)
            except Exception as e:
                log.exception('quitting due to uncaught exception')
                sys.exit(2)
            now = time.time()
            if now > stop_time:
                break
            else:
                print(random.choice('.-_\''), end='')
                sys.stdout.flush()
                time.sleep(30)
        # if we got here with no trouble, exit with success
        sys.exit(0)
    else:
        log.debug('%s disabled; not using', spec)
        sys.exit(3)
