import sys
import logging
import logging.config
from contextlib import contextmanager

def start_logging(c_logging):
    if c_logging:
        # https://docs.python.org/3/library/logging.config.html#logging-config-dictschema
        logging.config.dictConfig(c_logging)
    else:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
        logging.warning("no logging configured; "
                        "logging everything at DEBUG level to stderr")
    logging.captureWarnings(True)

def finish_logging():
    # if logging to a socket you have to do this
    logging.shutdown()

@contextmanager
def logging_handled(c_logging):
    start_logging(c_logging)
    try:
        yield
    finally:
        finish_logging()
