from itertools import islice
import re
import logging
import exchangelib
import keyring
from exchangelib import UTC, EWSDateTime, EWSTimeZone


class Processor:
    def __init__(self, account, *, from_, success, config):
        short_class_name = ''.join(x[0:2] for x in
                                   filter(None,
                                          re.split(r'(?=[A-Z])',
                                                   self.__class__.__name__)))
        short_name = '.'.join(['eb' if x == 'exchangebot' else x[0:2] for x in
                               filter(None,
                                      self.__class__.__module__.split('.'))] +
                              [short_class_name])
        self.log = logging.getLogger(short_name)
        self.account = account
        self.from_ = self.find_folder(from_)
        self.success = self.find_folder(success)
        self.config = config
        self._more_init()

    def _more_init(self):
        """More initialization tasks. Override in subclasses.

        I used keyword-only arguments for __init__, and it isn't clear
        how to use super() with them. If a way exists, all subclasses
        would have to use it. So this method is for subclasses to
        perform any initialization tasks that normally would go in
        __init__, and with a much simpler method signature.

        Subclasses should always call super() first in their
        definitions of this method.

        """
        # https://rhettinger.wordpress.com/2011/05/26/super-considered-super/
        assert not hasattr(super(), '_more_init')

    def find_folder(self, path):
        # FIXME i18n? This hardcoded name might be internationalized
        here = self.account.root / 'Top of Information Store'
        for entry in path:
            here = here // entry
        return here

    def process_one(self, message):
        """Process a mail message. Override in subclasses.

        If message is successfully dealt with, return True.

        """
        raise NotImplementedError(f"{self.__class__.__name__}.process_one")

    def success(self, message):
        self.log.info('success with message %s; moving to %r', message.id,
                      self.success)

    def process_some(self, n):
        self.process_all(islice(self.from_.all().iterator(), n))

    def process_all(self, gen=None):
        if gen is None:
            gen = self.from_.all().iterator()
        for message in gen:
            self.log.info('found message. sent %s  from %s  subject %s. processing.',
                          message.datetime_sent, message.sender,
                          message.subject)
            try:
                succeeded = self.process_one(message)
                if succeeded:
                    message.is_read = True
                    message.save()
                    message.move(self.success)
                    self.log.info('processing succeeded; moved message to %s',
                                  self.success)
                else:
                    self.log.warning('message not successfully processed; '
                                     'leaving message where it was found')
            except Exception as e:
                self.log.exception('unhandled exception during processing; '
                                   'leaving message where it was found')
                raise

