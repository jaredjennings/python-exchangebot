from pathlib import Path
from csv import writer
from bs4 import BeautifulSoup
from ...processor import Processor


def uncruft_nbsps(s):
    # Every space in this html file is written as an &nbsp;, which
    # decodes to "\xa0". But we want the normal sort of space.
    return s.replace('\xa0',' ')


class SaveHTMLTableProblem(Exception):
    pass


class SaveHTMLTable(Processor):
    def out_filename(self, message):
        safe_dt = message.datetime_sent.isoformat(
            timespec='seconds').replace(':','-')
        dirname = Path(self.config.get('output_directory', '.'))
        prefix = self.config.get('prefix', 'default')
        return dirname / f"{prefix}-{safe_dt}.csv"
    
    def process_one(self, message):
        for attachment in message.attachments:
            if attachment.name == self.config['attachment_name']:
                soup = BeautifulSoup(attachment.content, features='lxml')
                for t in soup.find_all('table'):
                    table_strings = [
                        # careful! stripped_strings will skip things
                        # with only spaces, resulting in missing
                        # fields where we should have empty fields.
                        [uncruft_nbsps(''.join(td.strings)).strip()
                         for td in tr.find_all('td')]
                        for tr in t.find_all('tr')]
                    if len(table_strings) < 1:
                        raise SaveHTMLTableProblem('degenerate table in input')
                    if len(table_strings[0]) < 1:
                        raise SaveHTMLTableProblem('degenerate table row in input')
                    if table_strings[0][0] == self.config['upper_left_cell_text']:
                        with open(self.out_filename(message), 'wt',
                                  newline='') as f:
                            w = writer(f)
                            for row in table_strings:
                                # first one is the header
                                w.writerow(row)
                        return True
                else:
                    raise SaveHTMLTableProblem('no table in attachment',
                                                attachment.name)
        else:
            raise SaveHTMLTableProblem('expected attachment not found')
