# from hypothesis import given, reject
# from hypothesis.strategies import text
import datetime
from ..processor import SaveHTMLTable, SaveHTMLTableProblem

import pytest

from hypothesis import given, assume, settings, HealthCheck
from hypothesis.database import ExampleDatabase
from hypothesis.strategies import text
from hypothesis.extra.lark import from_lark
from lark import Lark

# some Windows file management problem led to
# FileNotFoundError. Turning off the database fixed.
settings.register_profile('default', database=None)
settings.load_profile('default')

@given(x=text(max_size=245))
@settings(suppress_health_check=[HealthCheck.too_slow])
def test_arbitrary_contents(x, mocker):
    assume(x.replace('/', '') != '')
    assume(x.replace('.', '') != '')
    atch = mocker.Mock()
    # the name attribute is special so we can't just pass it into the
    # constructor. https://blog.tunarob.com/2017/04/27/mock-name-attribute/
    atch.configure_mock(
        name='has_a_table.html',
        content=x)
    message = mocker.Mock(
        attachments=[atch],
        datetime_sent=datetime.datetime(2019,10,31,10,0,0),
        sender='foo@example.com',
        subject='some subject')
    account = mocker.Mock()
    from_folder = mocker.Mock()
    success_folder = mocker.Mock()
    the_file = mocker.Mock()
    the_open = mocker.patch('builtins.open')
    # ASSUMPTION: the code under test uses a with statement to open
    # the output file
    the_open.return_value.__enter__ = mocker.Mock(return_value = the_file)
    mocker.patch.object(SaveHTMLTable, 'find_folder')
    # ---
    p = SaveHTMLTable(
        account, from_=from_folder, success=success_folder,
        config={
            'attachment_name': 'has_a_table.html',
            'upper_left_cell_text': 'foo',
        })
    try:
        p.process_one(message)
    except SaveHTMLTableProblem:
        pass


our_html_grammar = """
    start: html
    html: "<html>" body "</html>"
    body: "<body>" content "</body>"
    content: table | p | STRING
    p: "<p>" STRING "</p>"
    table: "<table>" table_kids* "</table>"
    table_kids: tr | STRING
    tr: "<tr>" td* "</tr>"
    td: "<td>" STRING "</td>"
    STRING: /a-z0-9 /+
"""

@given(html=from_lark(Lark(our_html_grammar)))
@settings(suppress_health_check=[HealthCheck.too_slow])
def test_arbitrary_html(html, mocker):
    atch = mocker.Mock()
    # the name attribute is special so we can't just pass it into the
    # constructor. https://blog.tunarob.com/2017/04/27/mock-name-attribute/
    atch.configure_mock(
        name='has_a_table.html',
        content=html)
    message = mocker.Mock(
        attachments=[atch],
        datetime_sent=datetime.datetime(2019,10,31,10,0,0),
        sender='foo@example.com',
        subject='some subject')
    account = mocker.Mock()
    from_folder = mocker.Mock()
    success_folder = mocker.Mock()
    the_file = mocker.Mock()
    the_open = mocker.patch('builtins.open')
    # ASSUMPTION: the code under test uses a with statement to open
    # the output file
    the_open.return_value.__enter__ = mocker.Mock(return_value = the_file)
    mocker.patch.object(SaveHTMLTable, 'find_folder')
    # ---
    p = SaveHTMLTable(
        account, from_=from_folder, success=success_folder,
        config={
            'attachment_name': 'has_a_table.html',
            'upper_left_cell_text': 'foo',
        })
    try:
        p.process_one(message)
    except SaveHTMLTableProblem:
        pass
