import datetime
from ..processor import SaveHTMLTable, SaveHTMLTableProblem

import pytest

happy_path_attachment_html = """\
<html>
  <body>
    <table>
      <tr>
        <td>Not the table</td>
        <td>you are looking for</td>
      </tr>
      <tbody>
        <tr>
          <td>Throwaway data:</td>
          <td>&nbsp;1</td>
        </tr>
    </table>
    <table>
      <tr>
        <td>Number</td>
        <td>Other&nbsp;Name</td>
        <td>Nickname</td>
        <td>Weird Date</td>
        <td>Wide&nbsp;Column&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>
      <tr>
        <td>00012345</td>
        <td>Fullofeels&nbsp;Hovercraft&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Fullofeels&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>10&#x2f;31&#x2f;2016</td>
        <td>Just&nbsp;down-&nbsp;there&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>
      <tbody>
      </tbody>
    </table>
</body>
</html>
"""

happy_path_written_csv = """\
Number,Other Name,Nickname,Weird Date,Wide Column\r
00012345,Fullofeels Hovercraft,Fullofeels,10/31/2016,Just down- there\r
"""





empty_field_attachment_html = """\
<html>
  <body>
    <table>
      <tr>
        <td>Not the table</td>
        <td>you are looking for</td>
      </tr>
      <tbody>
        <tr>
          <td>Throwaway data:</td>
          <td>&nbsp;1</td>
        </tr>
    </table>
    <table>
      <tr>
        <td>Number</td>
        <td>Other&nbsp;Name</td>
        <td>Nickname</td>
        <td>Weird Date</td>
        <td>Wide&nbsp;Column&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>
      <tr>
        <td>00012345</td>
        <td>Fullofeels&nbsp;Hovercraft&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Fullofeels&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>10&#x2f;31&#x2f;2016</td>
        <td><font face="courier new" size="2">
                      <nobr   id=l00874688>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </nobr>
                    </font></td>
      </tr>
      <tbody>
      </tbody>
    </table>
</body>
</html>
"""

empty_field_written_csv = """\
Number,Other Name,Nickname,Weird Date,Wide Column\r
00012345,Fullofeels Hovercraft,Fullofeels,10/31/2016,\r
"""


@pytest.mark.parametrize('attachment_content,expected_csv', [
    (happy_path_attachment_html, happy_path_written_csv),
    (empty_field_attachment_html, empty_field_written_csv)])
def test_happy_path(mocker, attachment_content, expected_csv):
    atch = mocker.Mock()
    # the name attribute is special so we can't just pass it into the
    # constructor. https://blog.tunarob.com/2017/04/27/mock-name-attribute/
    atch.configure_mock(
        name='table-laden.html',
        content=attachment_content)
    message = mocker.Mock(
        attachments=[atch],
        datetime_sent=datetime.datetime(2019,10,31,10,0,0),
        sender='foo@example.com',
        subject='some subject')
    account = mocker.Mock()
    from_folder = mocker.Mock()
    success_folder = mocker.Mock()
    the_file = mocker.Mock()
    the_open = mocker.patch('builtins.open')
    # ASSUMPTION: the code under test uses a with statement to open
    # the output file
    the_open.return_value.__enter__ = mocker.Mock(return_value = the_file)
    mocker.patch.object(SaveHTMLTable, 'find_folder')
    # ---
    p = SaveHTMLTable(
        account, from_=from_folder, success=success_folder,
        config={
            'attachment_name': 'table-laden.html',
            'upper_left_cell_text': 'Number',
        })
    p.process_one(message)
    # ...
    assert the_open.called
    csv_written = ''.join(c[0][0] for c in the_file.write.call_args_list)
    assert csv_written == expected_csv
