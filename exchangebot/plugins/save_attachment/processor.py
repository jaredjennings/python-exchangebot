from pathlib import Path
from ...processor import Processor

class SaveAttachmentProblem(Exception):
    pass

class SaveAttachment(Processor):
    def _more_init(self):
        self.output_directory = self.config['output_directory']
        self.prefix = self.config['filename_prefix']
        self.extension = self.config['extension']
        
    def out_filename(self, message):
        safe_dt = message.datetime_sent.isoformat(
            timespec='seconds').replace(':','-')
        return (Path(self.output_directory) /
                f"{self.prefix}-{safe_dt}{self.extension}")

    def save_as_file(self, filename, content):
        with open(filename, 'wb') as f:
            f.write(content)

    def process_one(self, message):
        for attachment in message.attachments:
            if attachment.name.endswith(self.extension):
                self.save_as_file(str(self.out_filename(message)),
                                  attachment.content)
                return True
        else:
            raise SaveAttachmentProblem('expected attachment not found')
