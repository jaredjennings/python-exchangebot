import pytest
import exchangelib
from io import BytesIO
from pprint import pprint
from datetime import datetime, timedelta
import pytz
import tzlocal

from ..processor import SaveAttachment, SaveAttachmentProblem

@pytest.fixture
def make_attachment_email(mocker):
    def make_message(from_='some_bot@us.example.com',
                     message_id='<b4a@us.example.com>',
                     datetime_sent=datetime(2020,12,2,12,2,3),
                     attachment=True,
                     attachment_name='attachment1.csv',
                     attachment_content='stuff'):
        Mock = mocker.Mock
        if attachment:
            attachments = [
                Mock(spec=exchangelib.FileAttachment,
                     content_type='text/plain',
                     content=attachment_content)
            ]
            # The name attribute is part of a Mock, but we need to
            # mock it because it is part of the object being mocked
            # that is used by the code under test
            attachments[0].configure_mock(name=attachment_name)
        else:
            attachments = []
        return Mock(subject=f'message',
                    sender=Mock(email_address=from_),
                    datetime_sent=datetime_sent,
                    message_id=message_id,
                    attachments=attachments)
    return make_message


@pytest.fixture
def make_save_attachment_processor(mocker):
    def make_processor(*, alert_exists=False):
        Mock = mocker.Mock
        account = Mock()
        from_folder = Mock()
        success_folder = Mock()
        mocker.patch.object(SaveAttachment, 'find_folder')
        mocker.patch.object(SaveAttachment, 'save_as_file')
        mocker.patch('exchangebot.jingle.keyring.get_password',
                     return_value='definitely a string, not None')
        s = SaveAttachment(
            account, from_=from_folder, success=success_folder,
            config={
                'output_directory': 'x:\\files_from_save1',
                'filename_prefix': 'save1',
                'extension': ".csv"
            })
        return s
    return make_processor


def test_happy_path(mocker, make_attachment_email,
                    make_save_attachment_processor):
    Mock = mocker.Mock
    m = make_attachment_email()
    msg = make_attachment_email(datetime_sent=datetime(2020,12,2,12,2,3))
    s = make_save_attachment_processor()
    # ------------------
    retval = s.process_one(msg)
    # ------------------
    s.save_as_file.assert_called_with(
        'x:\\files_from_save1\\save1-2020-12-02T12-02-03.csv',
        'stuff')


def test_no_attachment(mocker, make_attachment_email,
                    make_save_attachment_processor):
    Mock = mocker.Mock
    m = make_attachment_email()
    msg = make_attachment_email(attachment=False)
    s = make_save_attachment_processor()
    # ------------------
    with pytest.raises(SaveAttachmentProblem):
        retval = s.process_one(msg)
    # ------------------


def test_wrong_attachment(mocker, make_attachment_email,
                          make_save_attachment_processor):
    Mock = mocker.Mock
    m = make_attachment_email()
    msg = make_attachment_email(attachment_name='foo.txt')
    s = make_save_attachment_processor()
    # ------------------
    with pytest.raises(SaveAttachmentProblem):
        retval = s.process_one(msg)
    # ------------------


