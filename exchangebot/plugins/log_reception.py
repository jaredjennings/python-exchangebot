import logging

class LogReceptionProblem(Exception):
    pass

class LogReception(Processor):
    def _more_init(self):
        super()
        self.prefix = self.config['message_prefix']
        
    def process_one(self, message):
        self.log.info('%s. received a message with subject %r',
                      self.prefix, message.subject)
        return True
