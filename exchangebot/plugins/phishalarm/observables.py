from thehive4py.models import AlertArtifact

from dataclasses import dataclass
from typing import Tuple

@dataclass
class URL:
    href: str
    tags: Tuple[str]

    def hive(self):
        return AlertArtifact(dataType='url',
                             data=self.href,
                             tags=self.tags)

@dataclass
class File:
    contents: bytes
    name: str
    tags: Tuple[str]

    def hive(self):
        return AlertArtifact(dataType='file',
                             data=(self.contents, self.name),
                             tags=self.tags)

@dataclass
class Mail:
    address: str
    tags: Tuple[str]

    def hive(self):
        return AlertArtifact(dataType='mail',
                             data=self.address,
                             tags=self.tags)

@dataclass
class MailSubject:
    subject: str
    tags: Tuple[str]

    def hive(self):
        return AlertArtifact(dataType='mail_subject',
                             data=self.subject,
                             tags=self.tags)
