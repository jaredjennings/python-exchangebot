import logging
from base64 import b64encode
from datetime import datetime, timedelta
import pytz
import magic
from requests.exceptions import HTTPError
from thehive4py.api import TheHiveApi
from thehive4py.models import Alert, AlertArtifact
from thehive4py.query import And, Eq
from ...jingle import jingle
from .casetracker import CaseTrackerRequestError

# See https://github.com/TheHive-Project/TheHive4py/issues/136 and
# https://github.com/jaredjennings/TheHive4py/tree/file-like-objects-for-alert-artifacts.
# Support for file-like objects passed into artifacts has not been
# merged yet, and making a subclass of AlertArtifact to specialize its
# behavior didn't work because the existing code uses `type(thing) ==
# WhatIExpect` rather than `isinstance(thing, WhatIExpect)`. So we
# have to monkey-patch it.

def _flo_prepare_file_data(self, file_info):
    if isinstance(file_info, tuple):
        # caller opened file object; caller will close it
        file_object, filename = file_info
        mime = magic.Magic(mime=True).from_buffer(file_object.read())
        file_object.seek(0)
        encoded_string = b64encode(file_object.read())
    else:
        file_path = file_info
        # we open and close our own file
        with open(file_path, "rb") as file_artifact:
            filename = os.path.basename(file_path)
            mime = magic.Magic(mime=True).from_file(file_path)
            encoded_string = b64encode(file_artifact.read())
    return "{};{};{}".format(filename, mime, encoded_string.decode())

AlertArtifact._prepare_file_data = _flo_prepare_file_data

class HiveCaseTracker:
    def __init__(self, config):
        self.log = logging.getLogger('eb.pl.ph.hi')
        api_key = jingle(config, 'api key', 'endpoint',
                         'api_key_from_keyring_with_username')
        # cert gets passed into requests API as verify parameter; see
        # requests documentation for meaning
        self.api = TheHiveApi(config['endpoint'], api_key,
                              cert=config.get('cacerts_filename', True))


    def file_or_extend_alert(self, reporter, subject, received_time,
                             message_id, artifacts):
        # "If an alert with the same tuple type, source and
        # sourceRef already exists, TheHive will refuse to create
        # it." So try to find it first.
        try:
            # We are looking by the message id which should be unique
            # for every sent message across time. So any artifact we
            # can get from the message has already been gotten. The
            # only fact derivable from a new report of the same
            # message is that the new reporter has received that same
            # message; the only way that would be new info is if they
            # were BCC'd.
            id_ = self._find_existing_alert(message_id)
            self.log.info('Existing alert %s is about message id %s. '
                          'Skipping this duplicate report.', id_, message_id)
            # TODO: add recipient email address artifact
        except KeyError:
            # If it isn't a recent message, we want to know, because
            # lots of analyzers may only look in recent time (in fact,
            # the Mimecast recent recipients analyzer uses 48 hours as
            # its default window into the past); also, infrastructure
            # pointed at by links will go down over time, whether due
            # to the actions of attackers or of defenders.
            now = pytz.timezone('UTC').localize(datetime.utcnow())
            two_days_ago = now - timedelta(2,0,0)
            if received_time < two_days_ago:
                case_tags = ('>48h',)
            else:
                case_tags = ()
            alert = Alert(
                title=subject,
                description=f'A user has reported a message, received at {received_time}, as a potential phish, using PhishAlarm.',
                tlp=2, # amber
                severity=1, # low
                pap=1, # green
                type='phish_report',
                source='Exchange',
                sourceRef=message_id,
                artifacts=[x.hive() for x in artifacts],
                tags=case_tags)
            try:
                response = self.api.create_alert(alert)
                response.raise_for_status()
            except HTTPError as e:
                raise CaseTrackerRequestError() from e


    def _find_existing_alert(self, message_id):
        # not sure why find_first didn't find it
        try:
            response = self.api.find_alerts(query=And(
                Eq('type', 'phish_report'),
                Eq('source', 'Exchange'),
                Eq('sourceRef', message_id)))
            response.raise_for_status()
        except HTTPError as e:
            raise CaseTrackerRequestError() from e
        results = response.json()
        for res in results:
            return res['_id']
        else:
            raise KeyError(message_id)
