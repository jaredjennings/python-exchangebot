# -*- coding: utf-8 -*-
import re
from io import BytesIO
from exchangelib import ItemAttachment, FileAttachment
from collections import defaultdict
from ...processor import Processor
from .urlfinder import URLFinder
from .observables import File, URL, Mail, MailSubject
from .casetracker import CaseTrackerRequestError
from .hive import HiveCaseTracker

class PhishAlarmPotentialPhish(Processor):
    """Process Potential Phish emails from PhishAlarm®.

    """
    
    def _more_init(self):
        super()
        # FIXME: This is only half flexible, and therefore not very
        # testable. It looks like a case for *gulp* dependency
        # injection. The immediate surface problem is that if you
        # don't write the right configuration, it'll blow up, and the
        # error won't point you well to a fix.
        self.case_tracker = HiveCaseTracker(self.config['hive'])
        self.urlfinder = URLFinder(
            mimecast_config=self.config.get('mimecast'))

    def process_one(self, message):
        artifacts = []
        reported_message = None
        for att in message.attachments:
            # A Potential Phish email has the reported message
            # attached. As a message object, it is available
            # from exhchangelib as an ItemAttachment.
            if isinstance(att, ItemAttachment):
                # The message which was reported as a potential phish.
                reported_message = att.item
                artifacts.extend(self._glean(reported_message))
            elif isinstance(att, FileAttachment):
                # A Potential Phish email has the HTML body attached as
                # a plain text file, bodyhtml.txt.
                if (re.match(r'bodyhtml-.*\.txt', att.name)
                    and att.content_type == 'text/plain'):
                    artifacts.extend(
                        self.urlfinder.find_urls_in_html(att.content))
            else:
                pass
        # Rather than try to parse attacker-provided headers in the headers.txt
        # attachment, we take the header values from the server. In some cases
        # these are slightly touched; but this means the code parsing the
        # attacker-provided text is on the server, where patches should be
        # frequent, automatic, and any admin knows how to do them; rather than
        # here, where patching requires a developer to release and deploy a new
        # version of this software.
        if reported_message:
            subject = (repr(reported_message.subject)
                       if reported_message.subject
                       else '[no subject]')
            mid = reported_message.message_id
            received = reported_message.datetime_received
            reporter = message.sender.email_address
            for art in artifacts:
                if isinstance(art, Mail) and art.address == reporter:
                    # tuple -> set -> tuple: remove duplicates
                    art.tags = tuple(set(art.tags + ('reporter',)))
                    break
            else:
                artifacts.append(Mail(reporter, ('reporter',)))    
            try:
                self.case_tracker.file_or_extend_alert(
                    reporter, subject, received, mid, artifacts)
            except CaseTrackerRequestError as e:
                self.log.exception('Could not process due to case tracker problem')
                return False
            else:
                return True
        else:
            self.log.error('No message attached to phish report')
            return False


    def _glean(self, msg):
        tags = ('email', 'src:phish_report')
        artifacts = []
        # While we can get to properties of the message
        # easily, saving it as a .msg file is surprisingly not
        # simple at all. https://stackoverflow.com/q/33235626
        artifacts.append(File(BytesIO(msg.mime_content), 'reported_message.eml',
                              ('email',)))
        # Since the mime_content is allegedly reconstructed
        # and the source of truth when we are asking Exchange
        # is the properties fetchable by EWS, we use those.
        subject = msg.subject
        artifacts.append(MailSubject(msg.subject, tags))
        # avoid making duplicate artifacts in case the same address
        # appears in more than one place, by growing a set of tags per
        # email address
        _email_tags = defaultdict(lambda: set())
        # sender is a single address object; reply_to is a list, it seems
        for address_objects, pertinent_tags in (
                ([msg.sender], ('header-from',)),
                (msg.reply_to or [], ('header-reply-to',))):
            for ao in address_objects:
                a = ao.email_address
                _email_tags[a] |= set(pertinent_tags)
                _email_tags[a] |= set(tags)
        # it appears that the *_recipients attribute of a
        # message that doesn't have any of that kind of
        # recipients is None. use [] for iterability.
        for recipient_list, recipient_tags in (
                    (msg.to_recipients or [], ('header-to',)),
                    (msg.cc_recipients or [], ('header-cc',)),
                    (msg.bcc_recipients or [], ('header-bcc',))):
                for mb in recipient_list:
                    a = mb.email_address
                    _email_tags[a] |= set(recipient_tags)
                    _email_tags[a] |= set(tags)
        for a, ts in _email_tags.items():
            artifacts.append(Mail(a, tags=tuple(ts)))
        return artifacts
