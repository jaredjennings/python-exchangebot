import json
import base64
from ..processor import PhishAlarmPotentialPhish
import exchangelib
from io import BytesIO
from pprint import pprint
from datetime import datetime, timedelta
import pytz
import tzlocal

import pytest



@pytest.fixture
def make_phish(mocker):
    def make_message(subject='highly dubious!',
                     from_='shadowy@example.com',
                     reply_to=None,
                     to=('hapless@example.com',),
                     cc=(),
                     bcc=(),
                     received=None,
                     message_id='<f00@example.com>',
                     content=b'oo scary'):
        # if this gets much more complicated (e.g. html), we should
        # use the email module to build the mime content.
        headers = [f'From: {from_}']
        if cc:
            headers.append("Cc: " + ', '.join(cc))
        headers.append(f'Message-Id: {message_id}')
        # i guess you wouldn't ever see a bcc header would you?
        mime_content = b'\r\n'.join(x.encode('ascii') for x in headers) + \
            b'\r\n' + content
        # The received times that actually come from Exchange are in
        # local time, so we can't just use UTC here.
        if received is None:
            now = tzlocal.get_localzone().localize(datetime.now())
            received = now - timedelta(0, 5*60, 0)
        Mock = mocker.Mock
        return Mock(
            subject=subject,
            sender=Mock(email_address=from_),
            reply_to=None if reply_to is None else [Mock(email_address=reply_to)],
            to_recipients=[Mock(email_address=a) for a in to],
            cc_recipients=[Mock(email_address=a) for a in cc] if cc else None,
            bcc_recipients=[Mock(email_address=a) for a in bcc] if bcc else None,
            datetime_received=received,
            message_id=message_id,
            content=content,
            mime_content=mime_content)
    return make_message

@pytest.fixture
def make_phish_report(mocker, make_phish):
    def make_message(phish=None,
                     from_='one_of_ours@us.example.com',
                     message_id='<b4a@us.example.com>',
                     headers=b'the headers yo',
                     htmlbody=b'send me <i>your pw</i>',
                     uuid='a-uuid'):
        Mock = mocker.Mock
        if phish is None:
            phish = make_phish()
        attachments = [
            Mock(spec=exchangelib.ItemAttachment,
                 item=phish),
            Mock(spec=exchangelib.FileAttachment,
                 content_type='text/plain',
                 content=headers),
            Mock(spec=exchangelib.FileAttachment,
                 content_type='text/plain',
                 content=phish.content),
            Mock(spec=exchangelib.FileAttachment,
                 content_type='text/plain',
                 content=htmlbody)]
        # The name attribute is part of a Mock, but we need to mock it
        # because it is part of the object being mocked that is used by
        # the code under test
        attachments[1].configure_mock(name=f'headers-{uuid}.txt')
        attachments[2].configure_mock(name='body-{uuid}.txt')
        attachments[3].configure_mock(name='bodyhtml-{uuid}.txt')
        return Mock(subject=f'Potential Phish: {str(phish.subject)}',
                    sender=Mock(email_address=from_),
                    message_id=message_id,
                    attachments=attachments)
    return make_message


@pytest.fixture
def make_potential_phish_processor(mocker):
    def make_processor(*, alert_exists=False):
        Mock = mocker.Mock
        account = Mock()
        from_folder = Mock()
        success_folder = Mock()
        mocker.patch.object(PhishAlarmPotentialPhish, 'find_folder')
        mocker.patch('exchangebot.jingle.keyring.get_password',
                     return_value='definitely a string, not None')
        p = PhishAlarmPotentialPhish(
            account, from_=from_folder, success=success_folder,
            config={'hive': {'endpoint': 'https://localhost:9443/thehive',
                             'api_key_from_keyring_with_username': 'name'},
                    'mimecast': {'base_url': 'https://us-api.example.com/',
                                 'app_id': 'a-guid',
                                 'app_key_from_keyring_with_username': 'bla1',
                                 'access_key_from_keyring_with_username': 'bla2',
                                 'secret_key_from_keyring_with_username': 'bla3'},
                    })
        if alert_exists:
            # a fake case tracker would be better than reaching inside
            # p to mock something. doing this well would require
            # passing configured objects into the processor object
            # constructor, instead of configuration. TODO
            mocker.patch.object(
                p.case_tracker, '_find_existing_alert',
                Mock(return_value='id of alert that totally exists'))
        else:
            mocker.patch.object(
                p.case_tracker, '_find_existing_alert',
                Mock(side_effect=KeyError('foo')))
        return p
    return make_processor


def test_happy_path(mocker, make_phish, make_phish_report,
                    make_potential_phish_processor):
    Mock = mocker.Mock
    dubious_message = make_phish(content=b'send me your pw')
    msg = make_phish_report(phish=dubious_message)
    p = make_potential_phish_processor()
    # ------------------
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=201))))
    retval = p.process_one(msg)
    # ------------------
    assert p.case_tracker.api.create_alert.called
    alert_json = p.case_tracker.api.create_alert.call_args[0][0].jsonify()
    alert = json.loads(alert_json)
    b64_send_me = base64.b64encode(b'send me your pw').decode('UTF-8')
    assert len(alert['artifacts']) >= 4
    print(alert['artifacts'])
    assert any(('shadowy@example.com' in art['data'] and
                'header-from' in art['tags'])
               for art in alert['artifacts'])
    assert any(('one_of_ours@us.example.com' in art['data'] and
                'reporter' in art['tags'])
               for art in alert['artifacts'])
    assert retval is True


def test_reply_to_same_address(mocker, make_phish, make_phish_report,
                    make_potential_phish_processor):
    Mock = mocker.Mock
    dubious_message = make_phish(content=b'send me your pw',
                                 from_='sam.spade@shadowy.example.com',
                                 reply_to='sam.spade@shadowy.example.com')
    msg = make_phish_report(phish=dubious_message)
    p = make_potential_phish_processor()
    # ------------------
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=201))))
    retval = p.process_one(msg)
    # ------------------
    assert p.case_tracker.api.create_alert.called
    alert_json = p.case_tracker.api.create_alert.call_args[0][0].jsonify()
    alert = json.loads(alert_json)
    b64_send_me = base64.b64encode(b'send me your pw').decode('UTF-8')
    assert len(alert['artifacts']) >= 4
    print(alert['artifacts'])
    assert any(('sam.spade@shadowy.example.com' in art['data'] and
                'header-from' in art['tags'] and
                'header-reply-to' in art['tags'])
               for art in alert['artifacts'])
    assert any(('one_of_ours@us.example.com' in art['data'] and
                'reporter' in art['tags'])
               for art in alert['artifacts'])
    assert retval is True


def test_reply_to_different_address(mocker, make_phish, make_phish_report,
                    make_potential_phish_processor):
    Mock = mocker.Mock
    dubious_message = make_phish(content=b'send me your pw',
                                 from_='sam.spade@shadowy.example.com',
                                 reply_to='jojo@supercool.example.com',
                                 to=('jojo@supercool.example.com',))
    msg = make_phish_report(phish=dubious_message)
    p = make_potential_phish_processor()
    # ------------------
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=201))))
    retval = p.process_one(msg)
    # ------------------
    assert p.case_tracker.api.create_alert.called
    alert_json = p.case_tracker.api.create_alert.call_args[0][0].jsonify()
    alert = json.loads(alert_json)
    b64_send_me = base64.b64encode(b'send me your pw').decode('UTF-8')
    assert len(alert['artifacts']) >= 4
    pprint(alert['artifacts'])
    assert any(('sam.spade@shadowy.example.com' in art['data'] and
                'header-from' in art['tags'] and
                not 'header-reply-to' in art['tags'])
               for art in alert['artifacts'])
    assert any(('jojo@supercool.example.com' in art['data'] and
                'header-from' not in art['tags'] and
                'header-to' in art['tags'] and
                'header-reply-to' in art['tags'])
               for art in alert['artifacts'])
    assert any(('one_of_ours@us.example.com' in art['data'] and
                'reporter' in art['tags'])
               for art in alert['artifacts'])
    assert retval is True


def test_duplicate_report(mocker, make_phish, make_phish_report,
                          make_potential_phish_processor):
    Mock = mocker.Mock
    dubious_message = make_phish(content=b'send me your pw')
    msg = make_phish_report(phish=dubious_message)
    p = make_potential_phish_processor(alert_exists=True)
    # ------------------
    mocker.patch.object(p.case_tracker, '_find_existing_alert',
                        Mock(return_value='id of alert that totally exists'))
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=400))))
    retval = p.process_one(msg)
    # ------------------
    assert not p.case_tracker.api.create_alert.called
    assert retval is True
    
def test_no_subject(mocker, make_phish, make_phish_report,
                    make_potential_phish_processor):
    Mock = mocker.Mock
    dubious_message = make_phish(subject=None,
                                 content=b'send me your pw')
    msg = make_phish_report(phish=dubious_message)
    p = make_potential_phish_processor()
    # ------------------
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=201))))
    retval = p.process_one(msg)
    # ------------------
    assert p.case_tracker.api.create_alert.called
    alert_json = p.case_tracker.api.create_alert.call_args[0][0].jsonify()
    alert = json.loads(alert_json)
    assert alert['title'] == '[no subject]'


def test_old_message(mocker, make_phish, make_phish_report,
                     make_potential_phish_processor):
    Mock = mocker.Mock
    now = tzlocal.get_localzone().localize(datetime.now())
    a_week_ago = now - timedelta(7,0,0)
    dubious_message = make_phish(content=b'send me your pw',
                                 received=a_week_ago)
    msg = make_phish_report(phish=dubious_message)
    p = make_potential_phish_processor()
    # ------------------
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=201))))
    retval = p.process_one(msg)
    # ------------------
    assert p.case_tracker.api.create_alert.called
    alert_json = p.case_tracker.api.create_alert.call_args[0][0].jsonify()
    alert = json.loads(alert_json)
    b64_send_me = base64.b64encode(b'send me your pw').decode('UTF-8')
    assert retval is True
    assert '>48h' in alert['tags']


def test_link(mocker, make_phish, make_phish_report,
              make_potential_phish_processor):
    Mock = mocker.Mock
    dubious_message = make_phish(subject=None,
                                 content=b'send me your pw')
    msg = make_phish_report(phish=dubious_message,
                            htmlbody='foo <a href="http://example.com">bar</a>')
    p = make_potential_phish_processor()
    # ------------------
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=201))))
    retval = p.process_one(msg)
    # ------------------
    assert p.case_tracker.api.create_alert.called
    alert_json = p.case_tracker.api.create_alert.call_args[0][0].jsonify()
    alert = json.loads(alert_json)
    print(alert)
    assert any((art['data'] == 'http://example.com' and
                art['dataType'] == 'url') for art in alert['artifacts'])

def test_mimecast_protected_link(mocker, make_phish, make_phish_report,
              make_potential_phish_processor):
    Mock = mocker.Mock
    dubious_message = make_phish(subject=None,
                                 content=b'send me your pw')
    msg = make_phish_report(phish=dubious_message,
                            htmlbody='foo <a href="http://protect-za.mimecast.com/yfwutnwyfutnwfyutn">bar</a>')
    p = make_potential_phish_processor()
    # ------------------
    mocker.patch.object(p.case_tracker, 'api',
                        Mock(create_alert=Mock(
                            return_value=Mock(
                                status_code=201))))
    mocker.patch.object(p.urlfinder, 'mimecast_api',
                        Mock(decode_url=Mock(
                            return_value='http://evil.site.example.com')))
    retval = p.process_one(msg)
    # ------------------
    assert p.case_tracker.api.create_alert.called
    alert_json = p.case_tracker.api.create_alert.call_args[0][0].jsonify()
    alert = json.loads(alert_json)
    print(alert)
    assert any((art['data'] == 'http://evil.site.example.com' and
                art['dataType'] == 'url') for art in alert['artifacts'])
