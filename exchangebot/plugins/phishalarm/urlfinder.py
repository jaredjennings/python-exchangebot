import re
from io import BytesIO
from urllib.parse import urlparse, urljoin
import logging
from ...jingle import jingle
from .observables import URL, File
from mimecast_api import MimecastAPI
from bs4 import BeautifulSoup

class URLFinder:
    def __init__(self, *, mimecast_config=None):
        self.log = logging.getLogger('eb.pl.ph.uf')
        c = mimecast_config
        self.mimecast = bool(mimecast_config)
        if self.mimecast:
            # why does mimecast have three secrets...?
            self.mimecast_api = MimecastAPI(
                base_url=c['base_url'],
                access_key=jingle(c, 'Mimecast API app access key',
                                  'base_url',
                                  'access_key_from_keyring_with_username'),
                secret_key=jingle(c, 'Mimecast API app secret key',
                                  'base_url',
                                  'secret_key_from_keyring_with_username'),
                app_id=c['app_id'],
                app_key=jingle(c, 'Mimecast API app key',
                               'base_url',
                               'app_key_from_keyring_with_username'))

    def find_urls_in_html(self, html_content):
        """Yield some tuples with info about URLs in the html_content.

        Exactly how many is not specified, of course. There may be
        multiple links, or each link may need some analysis of its
        own, yielding multiple artifacts. Or there may be no links.

        """
        try:
            soup = BeautifulSoup(html_content, 'html.parser')
        except Exception as e:
            # soliloquy about attachment data: the exchangelib folks
            # tried their best to prevent me from having to get the
            # entire contents into RAM, but they didn't provide a seek
            # on the attachment's .fp, and the Hive API needs to read
            # it twice; and the fp is actually a context manager, but
            # the Hive API isn't guaranteed to use up the file data
            # within this context. (For alert artifacts it in fact
            # does; but not for case observables, at the end of 2019.)
            # So we just use the content and make a BytesIO out of it.
            #
            # note, we are only attaching the HTML body as its own
            # thing when it fails to parse; otherwise its exact
            # content is much less interesting and is best analyzed as
            # part of the mime_content.
            yield File(BytesIO(html_content), 'bodyhtml',
                       ('email', 'bodyhtml', 'parse failed'))
            self.log.exception('failed to parse HTML content; continuing without it')
        else:
            seen = set()
            for link in soup.find_all('a'):
                href = link.get('href')
                # TODO: get body text inside the <a> tag; if it is a
                # URL, capture it; if it is a different URL from the
                # href, tag it with something red. Challenge:
                # arbitrary tags, false positives, multiple
                # URL-looking things, Mimecast link protection messing
                # with text.
                if href and href not in seen:
                    yield from self.grok_url(href)
                    seen.add(href)

    def grok_url(self, url_text):
        upr = urlparse(url_text)
        if self.mimecast and re.match(r'protect-[a-z]+\.mimecast\.com', upr.netloc):
            # it is a mimecast link protect link.
            try:
                self.log.info('decoding Mimecast protected URL %s', url_text)
                decoded_url = self.mimecast_api.decode_url(url_text)
            except Exception as e:
                self.log.exception('decode failed; leaving un-decoded link in alert')
                yield URL(url_text, ('link','mimecast','decode-failed'))
            else:
                yield URL(decoded_url, ('link','mimecast-decoded'))
        else:
            # grokking found nothing but the original url.
            yield URL(url_text, ('link',))
