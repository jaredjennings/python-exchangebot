import getpass
from exchangelib import Configuration, Account, SSPI, DELEGATE, Credentials
import keyring

def account_from_mail_config(c_mail):
    server = c_mail['server']
    authn_method = c_mail['authn_method']
    if authn_method == 'sspi':
        config = Configuration(server=server,
                               auth_type=SSPI)
    elif authn_method == 'prompt_for_password':
        username = c_mail['username']
        password = getpass.getpass(f'{username} password: ')
        creds = Credentials(username, password=password)
        config = Configuration(server=server,
                               credentials=creds)
    elif authn_method == 'password_from_keyring':
        username = c_mail['username']
        password = keyring.get_password(server, username)
        if password is None:
            raise KeyError('no password found in keyring', server, username)
        creds = Credentials(username, password=password)
        config = Configuration(server=server,
                               credentials=creds)
    else:
        raise ValueError('unrecognized authn_method', authn_method, 'in config')
    return Account(
        primary_smtp_address=c_mail['primary_smtp_address'],
        config=config,
        access_type=DELEGATE)

