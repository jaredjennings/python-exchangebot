import keyring

def jingle(config, whatisit, service_key, username_key):
    """Try to get a secret from the keyring.

    whatisit is the natural-language name of this secret,
    e.g. "API key" or "auxiliary launch password."

    service_key is the key in the configuration under which the
    service name (e.g. a url) is stored; username_key is the key
    under which a designator valid in the context of the service
    (e.g. a username) is stored.

    jingle fetches the secret or throws an exception.

    """
    service = config[service_key]
    username = config[username_key]
    pw = keyring.get_password(service, username)
    if pw is None:
        raise KeyError(f'no {whatisit} found in keyring', service, username)
    return pw
